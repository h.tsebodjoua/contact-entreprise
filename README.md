# contact-entreprise

Développement d’une WEB API d’une gestion de contacts dans une entreprise.

## Lancer l'application

L'application se lance via la classe: ContactEntrepriseApplication.java dans un IDE

## Base de donnée

La base de donnée utilisée est H2.
On peut se connecter à la db via l'url: http://localhost:8081/h2-console.
Avec comme credentials, username: sa sans mot de passe.

## Tester l'application

L'application peut être testé  via Swagger-ui via le lien: http://localhost:8081/swagger-ui.html
ou alors  via Postman


